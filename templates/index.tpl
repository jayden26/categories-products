<ul>
{if count($categories) > 0}
    {foreach from=$categories item=rows}
        <li>#{$rows->getCategoryId()} {$rows->getCategoryName()} ({$rows->getCountCategories()})</li>
    {/foreach}
{else}
	<li>Categories not found.</li>
{/if}
</ul>
