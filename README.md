### ### PHP OOP Class/Methods CRUD ### ###

*About repo:*

Get Categories & Products using php , mysql , xml and using Smarty template engine (2.x
    http://www.smarty.net/download)

*Install:* 

* Download files
* Extract to local folder localhost/joboffer
* chmod templates_c 0777
* Edit config.php for using database
* Go to localhost/phpmyadmin and upload .sql file

It Works!

*PHP code class products:*

![2015-08-06_15-59.jpg](https://bitbucket.org/repo/7oKkrj/images/2444761600-2015-08-06_15-59.jpg)

*Return result:*

![2015-08-06_16-07_localhost-joboffer-tester.php.jpg](https://bitbucket.org/repo/7oKkrj/images/962761286-2015-08-06_16-07_localhost-joboffer-tester.php.jpg)

Get rows products and products price vat 21 %

*PHP code class categories:*

![2015-08-06_16-26_joboffer.jpg](https://bitbucket.org/repo/7oKkrj/images/1370059135-2015-08-06_16-26_joboffer.jpg)

*Return result:*

![2015-08-06_16-24.jpg](https://bitbucket.org/repo/7oKkrj/images/2793599322-2015-08-06_16-24.jpg)

Get rows categories and categories num rows