<?php

	header("Content-type:text/plain");

	$xmlData = file_get_contents('php://input');
	$xml = new SimpleXMLElement($xmlData);
    echo $xmlData;

	$html = new stdClass();
	require ('config.php');

	$smarty     =     new Smarty;
    $categories =     new Categories($db);
    $products   =     new Products($db);

        if ($xml->action == "getProducts"):

            $smarty->assign("products", $products->getProducts($xml->params));
            $html = "products.tpl";
            $smarty->display($html);

        elseif ($xml->action == "getCategories"):

            $smarty->assign("categories", $categories->getCategories());
            $html = "index.tpl";
            $smarty->display($html);

         else:

            exit;

        endif;